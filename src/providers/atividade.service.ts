import { Injectable } from '@angular/core';
//So importa Type Script
import { Atividade } from '../models/atividade'

@Injectable()
export class AtividadeService {



  private lista: Array<Atividade> = [];

  constructor() { 


     let atv1:Atividade = new Atividade();
     atv1.id = 1;
     atv1.titulo = "Trabalho Empresárial";
     atv1.data = "24/09/2018";
     atv1.descricao = "Talk about what do you do in your company ";
     atv1.tempo = "3 dias atrás" ;
     

     let atv2:Atividade = new Atividade();
     atv2.id = 2;
     atv2.titulo = "Trabalho TI";
     atv2.data = "26/09/2018";
     atv2.descricao = "Talk about what do you do in your company in TI sector";
     atv2.tempo = "10 dias atrás" ;


      this.lista.push(atv1);
      this.lista.push(atv2);


  }

  //metodo para retornar a nossa lista
  public read(): Array<Atividade> {

    return this.lista;

  }

  public delete(id: number): void {
  
     for(let i=0 ; i< this.lista.length; i++)
     {
        if(this.lista[i].id == id) {

         this.lista.splice(i,1);

        }
     
     }
  }
}