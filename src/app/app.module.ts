import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { LoginstudentComponent } from '../pages/loginstudent/loginstudent';
import { ForgotpasswordComponent } from '../pages/forgotpassword/forgotpassword';
import { HomepageComponent } from '../pages/homepage/homepage';
import { SigninComponent } from '../pages/signin/signin';
import { HomepagestudentComponent } from '../pages/homepagestudent/homepagestudent';
import { ListhomeworkComponent } from '../pages/listhomework/listhomework';
import { ActivityComponent } from '../pages/activity/activity';
import { AdmpainelComponent } from '../pages/admpainel/admpainel';
import { LoginadmComponent } from '../pages/loginadm/loginadm';
import { CadHomeworkComponent } from '../pages/cadhomework/cadhomework';
import { EditHomeworkComponent } from '../pages/edithomework/edithomework';
import { NativeAudio } from '@ionic-native/native-audio';
import { FeedbackComponent } from '../pages/feedback/feedback.component';
import { File } from '@ionic-native/file';
import { FeedbackAdmComponent } from '../pages/feedback-adm/feedback-adm.component';
import { MediaCapture } from '@ionic-native/media-capture';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { Media } from '@ionic-native/media';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//importação do modulo que vai permitir que relizamos cadastro no banco pelo app
import {FormsModule} from '@angular/forms';

//Serviços/modulo pro banco funiocnar a auth
import {AngularFireAuthModule} from '@angular/fire/auth';

import { IonicStorageModule } from '@ionic/storage';
import { LoginService } from '../providers/login.service';
import { AtividadeService } from '../providers/atividade.service';

// Initialize Firebase
  const config = {
    apiKey: "AIzaSyDDO2xihfu6rP0KtVsHLG49-sq9wDEYVGQ",
    authDomain: "fastenglish-12b6c.firebaseapp.com",
    databaseURL: "https://fastenglish-12b6c.firebaseio.com",
    projectId: "fastenglish-12b6c",
    storageBucket: "fastenglish-12b6c.appspot.com",
    messagingSenderId: "447222649022"
  };


@NgModule({
  declarations: [
    MyApp,
    LoginstudentComponent,
    ForgotpasswordComponent,
    HomepageComponent,
    SigninComponent,
    HomepagestudentComponent,
    ListhomeworkComponent,
    ActivityComponent,
    AdmpainelComponent,
    LoginadmComponent,
    CadHomeworkComponent,
    EditHomeworkComponent,
    FeedbackComponent,
    FeedbackAdmComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    FormsModule,
    AngularFireAuthModule,
    AngularFireStorageModule
  ],

  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginstudentComponent,
    ForgotpasswordComponent,
    HomepageComponent,
    SigninComponent,
    HomepagestudentComponent,
    ListhomeworkComponent,
    ActivityComponent,
    AdmpainelComponent,
    LoginadmComponent,
    CadHomeworkComponent,
    EditHomeworkComponent,
    FeedbackComponent,
    FeedbackAdmComponent

  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    NativeAudio, File, MediaCapture, Media, AtividadeService, StatusBar, SplashScreen
  ]
})
export class AppModule { }
