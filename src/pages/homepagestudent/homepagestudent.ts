import { Component, OnInit } from '@angular/core';
import {NavController , App, MenuController , AlertController}  from 'ionic-angular';
import {ListhomeworkComponent} from '../listhomework/listhomework';
import {HomepageComponent} from '../homepage/homepage';


@Component({
  
  templateUrl: './homepagestudent.component.html',

})
export class HomepagestudentComponent  {

  constructor(public nav:NavController , public alertCtrl:AlertController) { }

  public listhw():void {

    this.nav.push(ListhomeworkComponent);
 }

 public logout(){
   
 
     let alert = this.alertCtrl.create({
    title: 'exit application?',
    message: 'Logout?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Logout',
        handler: () => {
          console.log('Logout clicked');
          this.nav.setRoot(HomepageComponent);

        }
      }
    ]
  });
  alert.present();

   }

  }
    
 

 
 
