import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { HomepageComponent } from '../homepage/homepage';

import { NgForm } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth'

@Component({

  templateUrl: './signin.component.html',

})
export class SigninComponent {

  constructor(public alertController: AlertController, public navCtrl: NavController, public afAuth: AngularFireAuth) { }

  public salvar(form: NgForm) {

    let email = form.value.email;
    let senha = form.value.senha;


    this.afAuth.auth.createUserWithEmailAndPassword(email, senha)
      .then((result) => {
        result.user.updateProfile({
          displayName: '',
          photoURL: ''
        });

      })
      .catch((error) => { alert(error); })

    
      alert('Usuario Criado');
      this.navCtrl.push(HomepageComponent);


    }


}




