import { Component, OnInit  } from '@angular/core';
import {AlertController , NavController} from 'ionic-angular';
import {HomepageComponent} from '../homepage/homepage';


@Component({

  templateUrl: './forgotpassword.component.html',

})
export class ForgotpasswordComponent {

  constructor(private alert:AlertController , private navCtrl:NavController) { }

  public resetpassword(){

  const alert = this.alert.create({
    message: 'Reset Password request.Check your email.',
    buttons: ['OK']
    
  });
  this.navCtrl.setRoot(HomepageComponent);
  return alert.present();
  
}



}