import { Component, OnInit } from '@angular/core';

import { NavController, AlertController } from 'ionic-angular';

//importar modulos firebase
import { AngularFirestore } from '@angular/fire/firestore';

import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';

//importarr real time
import { Observable } from 'rxjs';

@Component({
  selector: 'app-feedback-adm',
  templateUrl: './feedback-adm.component.html'
})
export class FeedbackAdmComponent  {


  public lista: Observable<any[]>;
  public recording: boolean = false;
  public filePath: string;
  public fileName: string;
  public audio: MediaObject;
  public audioList: any[] = [];

  constructor(public nav: NavController, public alert: AlertController, public banco: AngularFirestore, private media: Media,
    private file: File) { 
      this.lista = banco.collection('homework', ref => ref.orderBy('date')).valueChanges();
  }

  public play(): void { 

  }
}