import { Component, OnInit } from '@angular/core';
import { LoadingController , NavController} from 'ionic-angular';
import {ListhomeworkComponent} from '../listhomework/listhomework';
import {SigninComponent} from '../signin/signin';
import {LoginadmComponent} from '../loginadm/loginadm';
import {LoginstudentComponent} from '../loginstudent/loginstudent';



@Component({

  templateUrl: './homepage.component.html'
 


})
export class HomepageComponent  {

  constructor(public navCtrl:NavController) { }

public loginstd(){

  this.navCtrl.push(LoginstudentComponent);
  
}

public signup(){


this.navCtrl.push(SigninComponent);

}

public loginadm(){

this.navCtrl.push(LoginadmComponent);

  

}




}