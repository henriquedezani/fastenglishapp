import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomepageComponent } from '../homepage/homepage';
//importar o ng form
import { NgForm } from '@angular/forms';

//importar o firestore
import { AngularFirestore } from '@angular/fire/firestore';
import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';

@Component({
  selector: 'app-cadhomework',
  templateUrl: './cadhomework.component.html',

})
export class CadHomeworkComponent {

  public recording: boolean = false;
  public recorded: boolean = false;
  public filePath: string;
  public fileName: string;
  public audio: MediaObject;
  public audioList: any[] = [];

  constructor(public banco: AngularFirestore, public navCtrl: NavController, private media: Media,
    private file: File) { }


  //metodo para salvar um novo documento no banco
  public salvar(form: NgForm) {

    let title = form.value.title;
    let description = form.value.description;
    let date = form.value.date;
    let status = form.value.status;
    let time_reply = form.value.time_reply;

    let homework = {

      title: title,
      description: description,
      date: date,
      status: true,
      time_reply: time_reply

    };

    this.banco.collection('homework').add(homework).then((ref => {
        let id = ref.id;
        this.banco.collection('homework').doc(id).update({ id: id })
      }
    ));

    this.navCtrl.pop();
  }

  public play(): void { 
    this.audio.play();
    this.audio.setVolume(1.0);
  }

  public startRecord(): void {
    this.fileName = 'record' + new Date().getTime() + '.3gp';
    this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
    this.audio = this.media.create(this.filePath);
    this.audio.startRecord();
    this.recording = true;
  }

  public stopRecord() {
    this.audio.stopRecord();
    // let data = { filename: this.fileName };
    // this.audioList.push(data);
    // localStorage.setItem("audiolist", JSON.stringify(this.audioList));
    this.recording = false;
    this.recorded = true;
  }

}