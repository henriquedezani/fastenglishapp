import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { EditHomeworkComponent } from '../edithomework/edithomework';
import { CadHomeworkComponent } from '../cadhomework/cadhomework';
import { HomepageComponent } from '../homepage/homepage';
import { AtividadeService } from '../../providers/atividade.service';
//Importar models
//import { Atividade } from '../../models/atividade';
import { ListhomeworkComponent } from '../listhomework/listhomework';
import { FeedbackComponent } from '../feedback/feedback.component';

//importar modulos firebase
import { AngularFirestore } from '@angular/fire/firestore'

//importarr real time
import { Observable } from 'rxjs';


@Component({
  selector: 'app-admpainel',
  templateUrl: './admpainel.component.html'

})
export class AdmpainelComponent {



  //public lista: Array<Atividade>; 

  public lista: Observable<any[]>;

  //invetar o modulo do firebase no construtor

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    //private service:AtividadeService , 
    public banco: AngularFirestore) {



    // this.lista = service.read();
    this.lista = banco.collection('homework').valueChanges();



  }

  public corrigir() {
    this.navCtrl.push(FeedbackComponent);
  }

  public criar() {
    this.navCtrl.push(CadHomeworkComponent);
  }

  public editar() {

    this.navCtrl.push(EditHomeworkComponent);

  }

  public cadhomework() {

    this.navCtrl.push(CadHomeworkComponent);

  }


  public logoutAdm() {
    let alert = this.alertCtrl.create({
      title: 'exit application?',
      message: 'Logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Logout',
          handler: () => {
            console.log('Logout clicked');
            this.navCtrl.setRoot(HomepageComponent);

          }
        }
      ]
    });
    alert.present();

    

  }

public delete(id: string): void {

let alert = this.alertCtrl.create({
      title: 'Delete HomeWork?',
     
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Delete clicked');
          this.banco.collection('homework').doc(id).delete();

          }
        }
      ]
    });
    alert.present();

  }





  public desativar(id: string)  {

    let alert = this.alertCtrl.create({
      title: 'Disable HomeWork?',
      
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Disable',
          handler: () => {
          console.log('Disable clicked');
          this.banco.collection('homework').doc(id).update({status : false})


        

          

          }
        }
      ]
    });

    alert.present();

  

  }

public ativar(id: string)  {

    let alert = this.alertCtrl.create({
      title: 'Enable HomeWork?',
      
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enable',
          handler: () => {
          console.log('Enable clicked');
          this.banco.collection('homework').doc(id).update({status : true})


        

          

          }
        }
      ]
    });

    alert.present();

  

  }
  

  

  }

