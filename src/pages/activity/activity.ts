import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, Events, Content , AlertController, NavController} from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio';
//importar o ng form
import { NgForm } from '@angular/forms';

//importar o firestore
import { AngularFirestore } from '@angular/fire/firestore';
//importarr real time
import { Observable } from 'rxjs';




@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html'
})
export class ActivityComponent  {

  public lista: Observable<any[]>;

  constructor(public nav: NavController, public alert: AlertController, public banco: AngularFirestore) { 


      // this.lista = service.read();
    this.lista = banco.collection('homework').valueChanges();}


    

}



