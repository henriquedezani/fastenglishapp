import { Component, OnInit } from '@angular/core';
import {NavController , AlertController} from 'ionic-angular';
import {ActivityComponent} from '../activity/activity';
import {HomepageComponent} from '../homepage/homepage';
import {AtividadeService} from '../../providers/atividade.service';
//Importar models
import {Atividade} from '../../models/atividade';
//Importar models
//import { Atividade } from '../../models/atividade';

//importar modulos firebase
import { AngularFirestore } from '@angular/fire/firestore'

//importarr real time
import { Observable } from 'rxjs';

@Component({
  selector: 'app-listhomework',
  templateUrl: './listhomework.component.html',
  
})
export class ListhomeworkComponent  {

   //public lista: Array<Atividade>; 

  public lista: Observable<any[]>;

  constructor(public nav:NavController , public alert: AlertController , private service:AtividadeService , public banco: AngularFirestore) { 

      
    // this.lista = service.read();
    this.lista = banco.collection('homework').valueChanges();


  }

  public responder(){


   this.nav.push(ActivityComponent);

     

  }

  public logout(){

        let alert = this.alert.create({
    title: 'exit application?',
    message: 'Logout?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Logout',
        handler: () => {
          console.log('Logout clicked');
          this.nav.setRoot(HomepageComponent);

        }
      }
    ]
  });
  alert.present();
    

  }

}