import {AdmpainelComponent} from '../admpainel/admpainel';
import { Component, OnInit , ViewChild } from '@angular/core';
import {NavController,AlertController } from 'ionic-angular';
import {ForgotpasswordComponent} from '../forgotpassword/forgotpassword';
import {HomepagestudentComponent} from '../homepagestudent/homepagestudent';
import {ListhomeworkComponent}  from '../listhomework/listhomework';
import {LoginService} from '../../providers/login.service';
import {Login} from '../../models/login';
import {HomepageComponent} from '../homepage/homepage';

//Importar para funcionar o form
import {NgForm} from '@angular/forms';

//importar o modulo de autenticação
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  templateUrl: './loginadm.component.html',

})
export class LoginadmComponent  {

  @ViewChild('email') email;
  @ViewChild('senha') senha;


  constructor(public nav:NavController , public atertCtrl:AlertController) {



   }


  public forgotpassword (){
   
     this.nav.push(ForgotpasswordComponent);


  }

  public login(){
   
   if (this.email.value == "admin" && this.senha.value == "admin") {


    let alert = this.atertCtrl.create({
    title: 'Bem vinda professora Lidiane ',
    buttons: ['OK']
  });
    alert.present();
       
    this.nav.setRoot(AdmpainelComponent);

   }

   else {

    let alert = this.atertCtrl.create({
    title: 'Login Failed',
    subTitle: 'your password or user was not found on the server.',
    buttons: ['OK']
  });
    alert.present();


   }

 


 
  }}