import { Component, OnInit , ViewChild } from '@angular/core';
import {NavController,AlertController } from 'ionic-angular';
import {ForgotpasswordComponent} from '../forgotpassword/forgotpassword';
import {HomepagestudentComponent} from '../homepagestudent/homepagestudent';
import {ListhomeworkComponent}  from '../listhomework/listhomework';
import {LoginService} from '../../providers/login.service';
import {HomepageComponent} from '../homepage/homepage';

import { NgForm } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth'; //sao serviços


@Component({

  templateUrl: './loginstudent.component.html',
 
})
export class LoginstudentComponent  {

  //@ViewChild('user') user;
  //@ViewChild('password') password;

  


  constructor(public nav:NavController , public atertCtrl:AlertController, public  afAuth: AngularFireAuth) {}

   public login(form: NgForm) : void 

   {

   

    let email = form.value.email;
    let senha = form.value.senha;

    this.afAuth.auth.signInWithEmailAndPassword(email, senha)
    .then((result) => { 

    const alert = this.atertCtrl.create({
    message: 'Bem Vindo Aluno',
    buttons: ['OK']
    
    
  });
   this.nav.setRoot(ListhomeworkComponent);
  return alert.present();
      
      
    })
    .catch((error) => {
      alert('Credenciais Incorretas');
    });

  
    
  }

  


  public forgotpassword (): void {
   
     this.nav.push(ForgotpasswordComponent);


  }

 /* public loginstud(){
   
   if (this.user.value == "aluno" && this.password.value == "aluno") {

        this.nav.setRoot(ListhomeworkComponent);

   }

   else {

         let alert = this.atertCtrl.create({
    title: 'Login Failed',
    subTitle: 'your password or user was not found on the server.',
    buttons: ['OK']
  });
    alert.present();


   }
 
  }*/
  
  }