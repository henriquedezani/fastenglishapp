import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ActivityComponent } from '../activity/activity';
import { HomepageComponent } from '../homepage/homepage';
import { AtividadeService } from '../../providers/atividade.service';
//Importar models
import { Atividade } from '../../models/atividade';
//Importar models
//import { Atividade } from '../../models/atividade';
import {FeedbackAdmComponent} from '../feedback-adm/feedback-adm.component';

//importar modulos firebase
import { AngularFirestore } from '@angular/fire/firestore'

//importarr real time
import { Observable } from 'rxjs';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html'
})
export class FeedbackComponent  {


     public lista: Observable<any[]>;


  constructor(public nav: NavController, public alert: AlertController, private service: AtividadeService, public banco: AngularFirestore) { 


      // this.lista = service.read();
    this.lista = banco.collection('homework').valueChanges();

  }

 
   public feedbackadm(){


    this.nav.push(FeedbackAdmComponent);



  }

}